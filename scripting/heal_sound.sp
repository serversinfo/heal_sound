#include <sdktools> 
public Plugin:myinfo =
{
 name = "Heal sound",
 author = "ShaRen",
 description = "играет звук когда игрок лечится",
 version =  "1.0.0"
};

public OnPluginStart()
{
	HookEvent("player_hurt", eV_player_hurt);
}

public Action:eV_player_hurt(Handle:event, const String:name[], bool:dontBroadcast)
{
	new dmg_health = GetEventInt(event, "dmg_health");
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	if (dmg_health < 0)
	{
		EmitAmbientSound("items/medcharge4.wav", NULL_VECTOR, client);
		PrintToChatAll("dmg_health < 0"); // когда заходишь в лечебку вообще ничего не происходит
	}
	if (dmg_health > 0)
	{
		EmitAmbientSound("items/medcharge4.wav", NULL_VECTOR, client);
		PrintToChatAll("dmg_health > 0"); // срабатывает только когда падаешь с высоты (естественным путем) если с помощью админки шлепать, то ничего не происходит
	}
}
